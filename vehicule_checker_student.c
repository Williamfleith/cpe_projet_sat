//By William FLEITH ON 22/04/2020

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

int send_msg(int id, int dlc, int data0, int data1, int data2)
{
	//the function input are the fram id, the frame dlc, and the bytes 0, 1 and 2

	int s; 
	struct sockaddr_can addr;
	struct ifreq ifr;
	struct can_frame frame;

	printf("CAN Sockets Demo\r\n");

	/* Create a socket. s contains the file descriptor of this socket */
	if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("Socket");
		return 1;
	}
	/* End creation socket */

	/* Retrieve the interface index for the interface name we want to use (can0, can1, vcan0, ...) */
	strcpy(ifr.ifr_name, "vcan0" );
	ioctl(s, SIOCGIFINDEX, &ifr);
	/* End of retrieving index */

	/* Bind the socket to the CAN Interface */
	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("Bind");
		return 1;
	}

	frame.can_id = id;
	frame.can_dlc = dlc;
	frame.data[0] = data0;
	frame.data[1] = data1;
	frame.data[2] = data2;


	if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
		perror("Write");
		return 1;
	}

	if (close(s) < 0) {
		perror("Close");
		return 1;
	}

	return 0;
	/* End of send a frame */

}
//main programm
int main(int argc, char **argv)
{

	send_msg(0x123, 2, 0, 0x01, 0); //right blink
	sleep(2);
	send_msg(0x123, 2, 0, 0x02, 0); //left blink
	sleep(2);
	send_msg(0x123, 2, 0x01, 0x00, 0); //low beam
	sleep(2);
	send_msg(0x123, 2, 0x02, 0x00, 0); //high beam
	sleep(2);
	send_msg(0x123, 2, 0, 0x00, 0); // stop beam
	sleep(2);
	send_msg(0x321, 3, 0x5B, 0, 0x09); //start engine, max speed, turn left
	sleep(9);
	send_msg(0x321, 3, 0x40, 0, 0);  //car go forward at 64% of max speed during 4 secondes
	sleep(4);
	send_msg(0x321, 3, 0, 0x64, 0); // car is braking until it stop.

}