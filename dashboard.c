//By William FLEITH ON 22/04/2020

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>



int filter_msg(int *pointeurSpeed, int *pointeurGear, int *pointeurMotor, signed char *pointeurDir)
{	
	int s, i; 
	int nbytes;
	struct sockaddr_can addr;
	struct ifreq ifr;
	struct can_frame frame;


    /* Create a socket. s contains the file descriptor of this socket */
	if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("Socket");
		return 1;
	}
    /* End creation socket */

	/* Retrieve the interface index for the interface name we want to use (can0, can1, vcan0, ...) */
	strcpy(ifr.ifr_name, "vcan0" );
	ioctl(s, SIOCGIFINDEX, &ifr);
	/* End of retrieving index */

    /* Bind the socket to the CAN Interface */
	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("Bind");
		return 1;
	}
    /* End of binding the socket */

    /* To read a frame,
	initialise a can_frame struct and call the read() method.
	This will block until a frame is available to read. */
	nbytes = read(s, &frame, sizeof(struct can_frame)); 

	if (nbytes < 0)
	{
		perror("Read");
		return 1;
	}
	
	if (frame.can_id == 0x80000C07)
	//Id for speed and gear, we make in the pointer the corresponding bytes 
	{
		*pointeurSpeed = frame.data[0];
		*pointeurGear = frame.data[1];
	}
	
	if (frame.can_id == 0x80000C06)
	//Id for motor speed, we make in the pointer the corresponding bytes 
	{
		*pointeurMotor = frame.data[0] + frame.data[1]*256;
	}
	
	if (frame.can_id = 0x00000321)
	// for the direction, we check the value of the first bit (the left one), signed binary
	//if it is a 0 it turn right and if it is 1 it is negativ and turns left
	// to do so we use a mask to check the value of the bit 
	{
        if ((frame.data[2] & 0b10000000) == 0b10000000)
        {
            *pointeurDir = '>';
        }

        if ((frame.data[2] & 0b10000000) == 0b00000000)
        {
            *pointeurDir = '<';
    	}	
		if (frame.data[2] == 0b00000000)
		{
			*pointeurDir = '^';
		}    	
	}
	if (close(s) < 0) 
	{
		perror("Close");
		return 1;
	}

	return 0;
}

int main(int argc, char **argv)
{
	int speed = 0;
	int gear = 0;
	int motor_speed = 0;
	char direction = ' ';


    while(1) 
    // loop while(1) to do it until we stop it
    {
		filter_msg(&speed, &gear,&motor_speed, &direction);
		//we call the function with the corresponding pointers
		printf("speed : %d km/h // gear : %d // motor speed : %d rpm // action : %c \n", 
			speed, gear, motor_speed, direction);

	}

}