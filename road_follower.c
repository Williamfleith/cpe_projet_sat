#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

int send_msg(int id, int dlc, int data0, int data1, signed char data2)
{

	int s; 
	struct sockaddr_can addr;
	struct ifreq ifr;
	struct can_frame frame;

	/* Create a socket. s contains the file descriptor of this socket */
	if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("Socket");
		return 1;
	}
	/* End creation socket */

	/* Retrieve the interface index for the interface name we want to use (can0, can1, vcan0, ...) */
	strcpy(ifr.ifr_name, "vcan0" );
	ioctl(s, SIOCGIFINDEX, &ifr);
	/* End of retrieving index */

	/* Bind the socket to the CAN Interface */
	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("Bind");
		return 1;
	}

	frame.can_id = id;
	if (dlc==2) {
		frame.can_dlc = dlc;
		frame.data[0] = data0;
		frame.data[1] = data1;
	}
	else if (dlc==3) {
		frame.can_dlc = dlc;
		frame.data[0] = data0;
		frame.data[1] = data1;
		frame.data[2] = data2;
	}


	if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
		perror("Write");
		return 1;
	}

	if (close(s) < 0) {
		perror("Close");
		return 1;
	}

	return 0;
	/* End of send a frame */

}


int receive_car_info(int id, int mask, int *vehicle_speed, int *gear_selection, int *motor_speed, signed char *steering)
{
	int s, i; 
	int nbytes;
	struct sockaddr_can addr;
	struct ifreq ifr;
	struct can_frame frame;

	if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("Socket");
		return 1;
	}

	strcpy(ifr.ifr_name, "vcan0" );
	ioctl(s, SIOCGIFINDEX, &ifr);

	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("Bind");
		return 1;
	}


	struct can_filter rfilter[1];

	rfilter[0].can_id   = id;
	rfilter[0].can_mask = mask;

	setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter));

	nbytes = read(s, &frame, sizeof(struct can_frame));

	if (nbytes < 0) 
	{
		perror("Read");
		return 1;
	}

	/* Start add my code */
	if (id == 0x80000C07)
	{
		*vehicle_speed = frame.data[0];
		*gear_selection = frame.data[1];
	}

	if (id == 0x80000C06)
	{
	*motor_speed = frame.data[0] + 256*frame.data[1];
	}

	if (id == 0x00000321)
	{
		if ((frame.data[2] & 0b10000000) == 0b10000000)
		{
			*steering = '>';
		}

		if ((frame.data[2] & 0b10000000) == 0b00000000)
		{
			*steering = '<';
		}
		if (frame.data[2]==0b00000000)
		{
			*steering = '^';
		}
	}


	if (close(s) < 0) {
		perror("Close");
		return 1;
	}

	return 0;
    /* End of read a frame */
}


int receive_msg_cam(int id, int mask)
{
	int s, i; 
	int nbytes;
	struct sockaddr_can addr;
	struct ifreq ifr;
	struct can_frame frame;

    /* Create a socket. s contains the file descriptor of this socket */
	if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("Socket");
		return 1;
	}
    /* End creation socket */

	/* Retrieve the interface index for the interface name we want to use (can0, can1, vcan0, ...) */
	strcpy(ifr.ifr_name, "vcan0" );
	ioctl(s, SIOCGIFINDEX, &ifr);
	/* End of retrieving index */

    /* Bind the socket to the CAN Interface */
	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) 
	{
		perror("Bind");
		return 1;
	}
    /* End of binding the socket */

    /* To set up a filter, initialise can_filter struct.
    Populate can_id and can_mask allows to filter frame by id. */
	struct can_filter rfilter[1];

	/* Print every frame betwen 0x100 and 0x1FF */
	rfilter[0].can_id   = id;
	rfilter[0].can_mask = mask;

	setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter));
    /* End of setting up filter */

    /* To read a frame,
	initialise a can_frame struct and call the read() method.
	This will block until a frame is available to read. */
	nbytes = read(s, &frame, sizeof(struct can_frame));

	if (nbytes < 0) 
	{
		perror("Read");
		return 1;
	}

	/* Start add my code */
    int road_value = frame.data[0];

	/* End add my code */


	printf("0x%03X [%d] ",frame.can_id, frame.can_dlc);

	for (i = 0; i < frame.can_dlc; i++)
		printf("%02X ",frame.data[i]);

	printf("\r\n");

	if (close(s) < 0) 
	{
		perror("Close");
		return 1;
	}

	return road_value;
    /* End of read a frame */
}

int where_to_go() {
    int road_value_00;
    int road_value_01;
    int road_value_02;
    int road_value_03;
    int road_value_04;
    int road_value_05;

    int table_road_value[50];
    int size_table = 6;
    int i;
    int max;
    table_road_value[0] = receive_msg_cam(0x80000C00, 0x1FFFFFFF);
    table_road_value[1] = receive_msg_cam(0x80000C01, 0x1FFFFFFF);
    table_road_value[2] = receive_msg_cam(0x80000C02, 0x1FFFFFFF);
    table_road_value[3] = receive_msg_cam(0x80000C03, 0x1FFFFFFF);
    table_road_value[4] = receive_msg_cam(0x80000C04, 0x1FFFFFFF);
    table_road_value[5] = receive_msg_cam(0x80000C05, 0x1FFFFFFF);

    max = 0;
    for (i=0; i<size_table; i++) 
    {
        if (table_road_value[i] > table_road_value[max]) 
        {
            max = i;
        }
    }
    return max;
}


int main(int argc, char **argv)
{
    int direction;
	int vehicle_speed = 0;
	int gear_selection = 0;
	int motor_speed = 0;
	char steering = ' ';

    while(1) {
        receive_car_info(0x80000C07, 0x1FFFFFFF, &vehicle_speed, &gear_selection, &motor_speed, &steering);
        printf("\nSpeed: %d km/h // Gear: %d // Motor speed: %d rpm // Direction : %c\n", vehicle_speed, gear_selection, motor_speed, steering);

        direction = where_to_go();
        printf("%d\n", direction);
        if (direction == 0)
        {
            if (vehicle_speed <= 45)
            {
                send_msg(0x321, 3, 0x20, 0x00, 15);
            }
            else 
            {
                send_msg(0x321, 3, 0x00, 0x10, 15);
            }
        }
        else if (direction == 1) 
        {
            if (vehicle_speed <= 45) 
            {
                send_msg(0x321, 3, 0x20, 0x00, 7);
            }
            else 
            {
                send_msg(0x321, 3, 0x00, 0x10, 7);
            }
        }
        else if (direction == 2) 
        {
            if (vehicle_speed <= 45) 
            {
                send_msg(0x321, 3, 0x20, 0x00, 3);
            }
            else 
            {
                send_msg(0x321, 3, 0x00, 0x10, 3);
            }
        }
        else if (direction == 3) 
        {
            if (vehicle_speed <= 45) 
            {
                send_msg(0x321, 3, 0x20, 0x00, -3);
            }
            else 
            {
                send_msg(0x321, 3, 0x00, 0x10, -3);
            }
        }
        else if (direction == 4) 
        {
            if (vehicle_speed <= 45) 
            {
                send_msg(0x321, 3, 0x20, 0x00, -7);
            }
            else 
            {
                send_msg(0x321, 3, 0x00, 0x10, -7);
            }
        }
        else if (direction == 5) 
        {
            if (vehicle_speed <= 45) 
            {
                send_msg(0x321, 3, 0x20, 0x00, -15);
            }
            else 
            {
                send_msg(0x321, 3, 0x00, 0x10, -15);
            }
        }
    }

}
