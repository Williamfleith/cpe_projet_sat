## COMPTE RENDU TP2A - FLEITH William - All rights reserved to CPE Lyon

## Introduction :

In this pratical session we will learn how to right a programm and tell the car to do a bunch of movment on the simulator avsim2D.

### 1 - Frames with semantic segmentation from the camera

#### For each area, describe what is seen by the vehicle :

- vcan0  00000C00   [8]  44 00 05 09 00 00 00 00 : 
	**Full left** : 0x44 in hexadecimal -> 68/100 of road, 0x05 in hexa -> 5/100 of yield and 0x09 in hexa -> 9/100 of crossing.
- vcan0  00000C01   [8]  2B 00 00 09 00 00 00 0B : 
	**Left** : 0x2B -> 43/100 of road, 0x09 -> 09/100 of crossing.
- vcan0  00000C02   [8]  33 00 00 06 00 00 00 0C : 
	**Middle left** : 0x33 -> 51/100 of road, 0x06 -> 6/100 of crossing.
- vcan0  00000C03   [8]  0F 00 00 00 00 00 00 06 : 
	**Middle right** : 0x0F -> 15/100 of road.
- vcan0  00000C04   [8]  06 00 00 00 00 00 00 00 : 
	**Right** : 0x06 -> 6/100 of road.
- vcan0  00000C05   [8]  2D 00 00 00 00 00 00 00 : 
	**Full right** : 0x2D -> 45/100 of road.

## 2 - Re-code vehicle_checker

In this part we will use the code that we did in TP1, with this code we will do a function that is sending an ID with its frame contening between 1 to 8 bytes of data. 
- **Function input will be** : ID, DLC(number of bytes), value of bytes 0, value of bytes 2, value of byte 3. 
- **Function output** : send the ID and its frame.

![image1](pictures/image1.png)

- In this image we can see that we call the function send_msg with the 5 argument to send the frame that we wanted. Between each call of the function we use a sleep(x) to stop the previous call of the function. 

- **Actuators** : ID : 0x123, DLC = 2, bytes : 
	- 00 01 : right blink
	- 00 02 : left blink
	- 01 00 : low beam
	- 02 00 : high beam
	- 00 00 : stop beam

- **Move the car** : ID : 0x321, DLC = 3, bytes : 
	- 0 : throttle 
	- 1 : brake
	- 2 : steering 

We are adjusting all those parameters to make the care go where we wanted.

So the programm is composed of a function send_msg and a main code that call this function as much as we need it. 


## 3 - Code a dashboard

In this part I decided to use and modifie the canfilter.c file that we used in TP1. With this code I did a function that took for arguments the 4 following pointers : pointeurSpeed, pointeurGear, pointeurMotor and pointeurDir.


The big advantage of pointers is that they can be sent to functions so that they directly modify a variable in memory, and not a copy. I used it to update in every frame the differents value. 

So in the main code we just need to call the function filter_msg in a while(1) (to do it until we stop it) and with the pointers it update the values. 


It print for every frame :

![image2](pictures/image2.png)


## 4 - Start a closed loop approach

Check the code road_follower.c, it use the previous functions with some modifications to follow roughtly the road. 


## COMPTE RENDU TP2B

## Practical Work 2 (part B - in autonomy)


La MISRA est un regroupement de règle de programmation. Elle concerne aussi la sécurité et la sûreté. Elle permet d'éviter et de retirer des codes, les faiblesses et les erreurs de celui-ci qui pourraient causer un dysfonctionnement dangereux. Cela permet lorsque le code peut porter atteinte à la sécurité d'un indivdu de normaliser celui-ci selon plusieurs règles.  


I must now change my road_follower.c code respecting MISRA rules.



